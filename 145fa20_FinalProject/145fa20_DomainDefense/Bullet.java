import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bullet here.
 * 
 * @author DGGASTON
 * @version 1.1
 */
public class Bullet extends Actor
{   
   /**
    * Do whatever the Bullet is needed to do.  
    */
   public void act() 
   {
       setLocation(getX()+8, getY());
       removeBullet();
   }// end method act 
       
    /**
    * Remove the Bullet if it reaches the end of the screen. 
    */
    private void removeBullet()
    {
        if (isAtEdge()) 
       {
           getWorld().removeObject(this);
       } // end if 
    }// end method removeBullet
    
}// end class bullet

