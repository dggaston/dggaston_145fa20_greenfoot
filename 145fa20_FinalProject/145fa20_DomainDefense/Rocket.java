import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rocket here.
 * 
 * @author DGGaston
 * @version 1.1
 */
public class Rocket extends Actor
{
    /**
     * Act - do whatever the Rocket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkKeyPress(); 
        makeScore(); 
    }  // end method act 
    
    /**
     * Check if a key has been pressed and act accordingly 
     */
    private void checkKeyPress() 
    {
        if (Greenfoot.isKeyDown("up"))
        {
            setLocation(getX(), getY()-8); 
        }// end if 
        
        if (Greenfoot.isKeyDown("down"))
        {
            setLocation(getX(), getY()+8); 
        } // end if 
        
        if(Greenfoot.isKeyDown("space"))
        {
            shoot(); 
        } // end if 
    } // end method checkKeyPress
    
    /**
     * Shoot a bullet from the rocket 
     */
    private void shoot()
    {
       if ("space".equals(Greenfoot.getKey()))
       {
        getWorld() .addObject(new Bullet(), getX(), getY());
       } // end if 
    }// end method shoot
   
    /**
     *  Remove the star from the world at give the player 32 points and play sound 
     */
    private void makeScore()
    {
     if (isTouching(Star.class))
     {
         Greenfoot.playSound("starcollection.mp3");
         removeTouching(Star.class);
         Space space = (Space)getWorld();
         space.addScore(+32); 
     } //end if 
       
    } // end method makeScore 
   
} // end class Rocket
