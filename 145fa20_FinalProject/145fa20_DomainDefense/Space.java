import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (DDGaston) 
 * @version (1.0)
 */
public class Space extends World
{
    private int score; 
    private int health; 
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Space()
    {   
        super(1000, 350, 1); 
        prepare ();
        setPaintOrder ( Domain.class ); 
        score = 0; 
        health = 100; 
        showScore(); 
        showHealth();
        showText("DOMAIN DEFENSE" , 500 , 15); // Put the title of the game in the top center of the world 
    } // end constructor 
    
    /**
     * Prepare to world for the start of the program
     */
    private void prepare ()
    {
        Rocket rocket = new Rocket(); 
        addObject(rocket,300,175);
        Domain domain = new Domain();
        addObject(domain, 100 ,175);
    } // end method prepare 
    
    /**
     * Making Comets appear at randome intervals
     */
    
    public void act ()
    {
        RandomComet();
        RandomStar(); 
    } // end method act 
    
    /**
     * Add points 
     */
    public void addScore (int points)
    {
        score = score + points; 
        showScore();
    } // end method addScore 
    
    /**
     * Show the score in the world 
     */
    private void showScore()
    {
        showText("Score:" + score, 500, 50 ); 
    } // end method showScore 
    
    /**
     * Show the health of the Domain and play sound endGame
     */
    public void subHealth (int healths )
    {
        health = health + healths;
        showHealth();
        if (health <= 0) 
        {   Greenfoot.playSound("endGame.mp3");
             showText( "GAME OVER!    SCORE:" + score , 500 , 175 ); 
             Greenfoot.stop();                    
        } // end if
    } // end method showHealth
    
    /**
     * Display the health in the world 
     */
    private void showHealth ()
    {
       showText("Health:" + health, 150 , 50 );
    } // end method showHealth
     
    /**
     * Spawn comets at random intervals within the world  
     */
    private void RandomComet()
    {
        if ( Greenfoot.getRandomNumber(100) < 1 ) 
        {
            addObject( new Comet(), 1000 , Greenfoot.getRandomNumber(340)+20); 
        } // end if 
    } // end method Random Comet
    
    /**
    * Spawn Stars at random intervcals within the world 
    */
    private void RandomStar()
    {
        if ( Greenfoot.getRandomNumber(500) < 2 ) 
        {
            addObject( new Star(), 1000 , Greenfoot.getRandomNumber(340)+20); 
        } // end if 
    } // end method Random Star 
} // End Space
