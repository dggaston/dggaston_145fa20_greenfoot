import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Comet here.
 * 
 * @author DGGaston 
 * @version 1.1
 */
public class Comet extends Actor
{
    /**
     * Act - do whatever the Comet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setLocation(getX()-3 , getY()); 
        checkCollision();
        removeComet(); 
         
    }  // end method act   

    /**
     * Remove the Comet if it collides with the bullet and explode  
     */
    private void checkCollision()
    {   
        if  ( isTouching(Rocket.class)) 
        { 
          Greenfoot.playSound("endGame.mp3");
          Space space = (Space)getWorld();
          getWorld().showText (" GAME OVER ! " , 500, 175 ); 
          Greenfoot.stop();
         }// end if 
    } //end method checkCollision  
    
    /**
     * Remoove the comet when the bullet reaches it 
     */
    private void removeComet()
    {
      if (isTouching(Bullet.class))
      {
         Greenfoot.playSound("cometdelete.mp3");
         removeTouching(Bullet.class);
         getWorld().removeObject(this);
      }//end if
    } // end method removeComet
    
} // end class Comet 


















