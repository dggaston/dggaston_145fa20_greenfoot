import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Domain here.
 * 
 * @author (DGGASTON) 
 * @version (1.1)
 */
public class Domain extends Actor
{  
    
     /**
     * Act - do whatever the Domain wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
     impact(); 
    } // end method act 
    
    /**
     *  If the comet reaches the domain delete the comet and subtract health 
     *  Play sound when the comet is deleted 
     */
     private void impact()
    {
        if (isTouching(Comet.class))
        {
            Greenfoot.playSound("impactsound.mp3");
            removeTouching(Comet.class);
            Space space = (Space)getWorld();
            space.subHealth(-13); 
        } // end if 
    } // end method impact 
    
} // end class Domain 
