import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Bacteria fload along in the bloodstream. They are bad. Best to destroy
 * them if you can.
 * 
 * @author Michael Kölling
 * @version 0.1
 */
public class Bacteria extends Actor
{
    private int bacteriaSpeed; // Exercise  5.18   
   
    /**
     * Excercise 5.19 - 20 generates a random vaule to the speed variable. In a range from 1 to 3, 
     * inclusively. 
     */
    public Bacteria()
    {
        bacteriaSpeed = Greenfoot.getRandomNumber(6);
    } // end constructor 

    /**
     * Float along the bloodstream at random speeds between 1 and 3, slowly rotating.
     */
    public void act() 
    {
        setLocation(getX()- bacteriaSpeed, getY());
        turn(1);

        if (getX() == 0) 
        {
            Bloodstream bloodstream = (Bloodstream)getWorld();
            bloodstream.addScore(-15); 
            bloodstream.removeObject(this); 
        } // end if 
    } // end method act 
} // end class bacteria 
