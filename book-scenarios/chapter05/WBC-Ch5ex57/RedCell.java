import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RedCell here.
 * 
 * @author dggaston
 * @version 1.0
 */
public class RedCell extends Actor
{
    private int redCellSpeed; 
    
    public RedCell()
    {
        redCellSpeed = Greenfoot.getRandomNumber(8); 
    }// end method RedCell
        
    /**
     * Act -- Make the red cells move just like bacteria. Right to left at variable speed, slowly 
     * rotating. 
     * The range of speed of is between 1 and 2 
     */
    public void act() 
    {
      setLocation(getX()- redCellSpeed, getY());
        setRotation(Greenfoot.getRandomNumber(15));
    } // end method act    
} // end class RedCell 
