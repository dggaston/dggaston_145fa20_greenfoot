import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Lining are objects at the edge of the vein.
 * 
 * @author Devin Gaston
 * @version 1.0
 */
public class Lining extends Actor
{
    /**
     * Excercise 5.7 
     * The act method is moving the lining through the world at a pace of -1 cells. The if 
     * statements is then removing the lining once the object reaches the end of the world. 
     */
    public void act() 
    {
        move(-2); // Exercise 5.5. Making the lining continuously move left through the world

        setLocation(getX()-2, getY()); // Excercise 5.6 Make the Lining objets disappear 
                                       //when they reach they reach the edge of the screen 
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if
        
        
    }  // end act 
} // end class 
