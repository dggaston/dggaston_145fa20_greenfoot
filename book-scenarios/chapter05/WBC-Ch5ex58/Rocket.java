import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is a white blood cell. This kind of cell has the job to catch 
 * bacteria and remove them from the blood.
 * 
 * @author Michael Kölling
 * @version 0.1
 */
public class Rocket extends Actor
{
    /**
     * Act: move up and down when cursor keys are pressed.
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision(); // Excercise 5.13
         
    } // end act
    
     /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
       {
          setLocation(getX(), getY()-8);
       } // end if 
        
       if (Greenfoot.isKeyDown("down")) 
       {
           setLocation(getX(), getY()+8);
       } // end if
        
       if ( Greenfoot.isKeyDown("left"))
       {
           move(-4);
       }  // end if 
       
       if (Greenfoot.isKeyDown ("right") )
       {
           move(4); 
       }// end if 
    } // end checkKeyPress
    
   
    /**
     * Check wheter we are touching bacteria  or a Virus 
     * Exercise 5.13-5.17
     */
    private void checkCollision()
    {
        if (isTouching(GoldenBall.class))
        {
        Greenfoot.playSound("whiteCellEat.wav");    
        removeTouching(GoldenBall.class);
        Space space = (Space)getWorld(); 
        space.addScore(20);
        } // end if 
        
        if (isTouching(Astroid.class))
        {
         Greenfoot.playSound("virusDetected.wav");   
         Space space = (Space)getWorld(); 
         space.addScore(-100);
         removeTouching(Astroid.class);
        } // end if 
    } // end checkCollision
} //end Class WhiteCell


     
