import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The bloodstream is the setting for our White Blood Cell scenario. 
 * It's a place where blood cells, bacteria and viruses float around.
 * 
 * @author Devin Gaston
 * @version 1.1
 */
public class Space extends World
{
    public int score; 
    public int time; 
    
    /**
     * Constructor : Prepare the world 
     *               Set the Border class Image on top 
     */
    public Space()
    {    
        super(780, 360, 1); 
        prepare();
        score = 0; 
        setPaintOrder ( Border.class ); 
        showScore(); 
        time = 2001; 
        showTime(); 
  
    } // end constructor

    /**
     * Create new floating objects at irregular intervals.
     */

    public void act()
    {
        if (Greenfoot.getRandomNumber(100) < 3)
        {
            addObject(new GoldenBall(), 779, Greenfoot.getRandomNumber(360));
        } // end if 

        //if (Greenfoot.getRandomNumber(100) < 10)
        //{
        //    addObject(new RedCell(), 779, Greenfoot.getRandomNumber(360));
        //} // end if

        // Exercise 5.8 
        if (Greenfoot.getRandomNumber(100) < 3)
        { 
            addObject(new Lining(), 779,0);
        } // end if 

        if (Greenfoot.getRandomNumber(100) < 3)
        {
            addObject(new Lining(), 779,359);
        } // end if 
        // Exercise 5.10
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Astroid(), 779,Greenfoot.getRandomNumber(360));
        } // end if 
        countTime(); 
        
    } // end method Act 
  /**
   * When the time runs out show the following... 
   */
  private void showEndMessage() 
  {
      showText("Time is up - you WIN! " , 400 , 250);
      showText("Your final score: " + score + "points" , 400 ,200);
    } // end method showEndMessage
  
    /**
     * Define a new private method called countTime that decrements the time by 1 every act 
     * cycle. 
     */
    private void countTime()
    {
        showTime(); 
    } // end method countTime
    
    /**
     * Decrement time by one each act cycle, starting at 2000, if the time reaches zero, 
     * then the game is over. 
     * Show the time within the world 
     */
    private void showTime()
    {
        time--; 
       showText("Time: " + time, 80, 50); 
       if(time == 0)
       {
         Greenfoot.stop(); 
         showEndMessage();
        }// end if 
    } // end method showTime 
    
    /**
      * Adds some the the points to the score. 
      * Ends the game when the score is < 0.  
      */

    public void addScore(int points)
    {
        score = score + points; 
        showScore();
       if (score < 0)
       {
         Greenfoot.playSound("game-over.wav");   
         Greenfoot.stop();    
        } // end if 
    } // end method addScore

    
    /**
     * Show the score within the world. 
     */
    private void showScore()
    {
        showText("Score: " + score, 80, 25);
    } // end method showScore 
    
    /**
     * Prepare the world for the start of the program. In this case: Create
     * a white blood cell and the lining at the edge of the blood stream.
     */
    private void prepare()
    {
        //Exercise 5.26
        Border border = new Border ();
        addObject(border, 0 ,180);
        Border border2 = new Border (); 
        addObject(border2 , 770 , 180);

        Rocket whitecell = new Rocket();
        addObject(whitecell, 121, 179);
        Lining lining = new Lining();
        addObject(lining, 126, 1);
        Lining lining2 = new Lining();
        addObject(lining2, 342, 5);
        Lining lining3 = new Lining();
        addObject(lining3, 589, 2);
        Lining lining4 = new Lining();
        addObject(lining4, 695, 5);
        Lining lining5 = new Lining();
        addObject(lining5, 114, 359);
        Lining lining6 = new Lining();
        addObject(lining6, 295, 353);
        Lining lining7 = new Lining();
        addObject(lining7, 480, 358);
        Lining lining8 = new Lining();
        addObject(lining8, 596, 359);
        Lining lining9 = new Lining();
        addObject(lining9, 740, 354);
    }
} // end class
