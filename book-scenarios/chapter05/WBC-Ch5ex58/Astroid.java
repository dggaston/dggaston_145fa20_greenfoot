import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A virus object that moves left through the world while rotating left. 
 * 
 * @author dggaston
 * @version 1.0
 */
public class Astroid extends Actor
{
    /**
     * Act - move left through the world, and rotate.
     */
    public void act() 
    {
        // Exercise 5.10
       setLocation(getX()-5 , getY());
       
       turn(-1);
       
       if (getX() == 0)
       {
           Space bloodstream = (Space)getWorld(); 
           bloodstream.removeObject(this); 
        }// end if 
    }// end act 
}// end class 
