import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

/**
 * The CrabWorld is the place where crabs and other creatures live. 
 * It creates the initial population.
 *  
 * @author dggaston@email.uscb.edu
 * @version 145fa20_lab3
 */
public class CrabWorld extends World
{
    /* FIELD(S) */

    /* For Exercise 4.31: Add a time counter to the world
     * (it makes more sense for the CrabWorld to keep track of time...
     *  it's not like a crab wears a watch or anything, right?)
     */ 
    private int timeCounter; 
    private boolean crabDefenseStatus; // CrabWorld "has-a" status for the crab
    private Crab myCrab;               // Crabworld "has-a" reference to the crab object 
    
    /* CONSTRUCTOR(S) */
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        /* 
         * Note that a call to the superclass constructor (if needed) 
         * must come before all other statements in the subclass constructor
         */
        super(560, 560, 1);

        /* For Exercise 4.32: Initialize timeCounter for countdown */
        timeCounter = 500; 
        crabDefenseStatus = true; // crab is "on defensse" for the start of the game 
        
        // prepare the world
        prepare();
    } // end CrabWorld constructor

    /* METHOD(S) */
    /**
     * Enables the CrabWorld to update its state from one cycle to the next
     */
    public void act()
    {
        /* For Ex. 4.32: Decrement the timeCounter variable */
        timeCounter = timeCounter - 1; // also could code as:
        // timeCounter--;
        // or
        // timeCounter -= 1;

        /*
         * For Ex. 4.33 and 4.34:
         * The showText method is described in the Greenfoot API here on the web: 
         * 
         * https://www.greenfoot.org/files/javadoc/greenfoot/World.html#showText-java.lang.String-int-int-
         * 
         * Note that the method takes 3 arguments: 
         * 1) a String for a text message to be displayed on the screen at 
         *    the position indicated by the following values:
         * 2) an int x-coordinate 
         * 3) an int y-coordinate
         */
        showText("Time left: " + timeCounter, 100, 40); // note the use of + as a concatenation operator
        /*                                               
         * For Ex. 4.32:
         * If the time counter reaches zero, play a "time is up" sound
         * and end the game 
         */
        //if ( timeCounter <= 0 )
        //{
         //   showText("Time's up!", 100, 40); // not required; this is a "bonus" 
         //  Greenfoot.playSound("alarm.mp3"); // play alarm sound//Greenfoot.stop(); // ends the game
       // } // end if

    } // end method act

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        // add the crab
        myCrab = new Crab();
        addObject(myCrab, 231, 203);

        // add ten worms, in random locations
        /*Worm worm = new Worm();
        addObject(worm, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm2 = new Worm();
        addObject(worm2, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm3 = new Worm();
        addObject(worm3, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm4 = new Worm();
        addObject(worm4, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm5 = new Worm();
        addObject(worm5, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm6 = new Worm();
        addObject(worm6, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm7 = new Worm();
        addObject(worm7, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm8 = new Worm();
        addObject(worm8, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm9 = new Worm();
        addObject(worm9, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        Worm worm10 = new Worm();
        addObject(worm10, Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        */
        
        /*
         * All counter=controlled loops, regardkess if whether it's a fir loop, or a do-while 
         * loop, require 3 things: 
         * 
         * 1) a counter varible
         * 2) loop-continuation condition
         * 3) a means of updating the counter varible such that the loop eventually exits 
         */
        /*
         * When to use each type of loop? (After all, any repetitiion 
         * logic can be implemented using any repetitiion statement
         *  --i. e., for loop can be coded as a while loop and vice versa) 
         *  
         *  1) If you know EXACTLY how many repetitions are required, use 'for' loop
         *  2) If you're usre how many repetition are needed AND you want to allow for the possiblility that your loop 
         *     might not execute EVEN ONCE 
         *  3) IF you're unsure how many repetitions are needed BUT you need the loop to 
         *  ecevcute at least once, code 'do-while' loop 
         */
        //decvlare and initialize counter varible 
        int wormIndex = 0;
        //do { 
          //  addObject( new Worm(), Greenfoot.getRandomNumber(531) + 15, Greenfoot.getRandomNumber(531) + 15);
          //  wormIndex++; // update the counter 
        //} while ( wormIndex < 10 ); // dont forget te semicolon!
        
        for ( int rowIndex = 0; rowIndex < 5; rowIndex++ ) 
        {
            for ( int colIndex = 0; colIndex < 6; colIndex++)
            {
                // replace 'colIndec' and 'rowindex' with expressions that 
                // will produce appropriate x and y cordinate calues such 
                // that the 'grid' of worms takes up more of the screen 
                
                // dependt varible = "slope" * independent variable + "intercept"
                addObject ( new Worm(),100 + (115*colIndex) , 100 + (93 *  rowIndex)  ); 
            } // end INNER for (each iteration corresponds to a COLUMN for the CURRENT ROW)
        } // end outter for 
        
        //define while loop and loop-continuation condition
        // (as long as the loop-continuation condition is true, then
        // the statements that make up the loop will execute) 
       
        //while (woreIndex < 10)
        //{   
        //    addObject( new worm(), Greenfoot.getMouseInfo(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        //    wormIndex++; // If you do not update the counter you will have an INFINITE LOOP! 
        //}// end while loop 
        
        //for (int WormIndex = 0 ; wormIndex < 10 ; wormIndex++ )
        //{
        //     addObject( new worm(), Greenfoot.getMouseInfo(531) + 15, Greenfoot.getRandomNumber(531) + 15);
        //} // end for loop 
        
        // add the three lobsters
        Lobster lobster = new Lobster();
        addObject(lobster, 334, 65);
        Lobster lobster2 = new Lobster();
        addObject(lobster2, 481, 481);
        Lobster lobster3 = new Lobster();
        addObject(lobster3, 79, 270);
    } // end method prepare  
    
    /**
     * This is an examoke of a mutator ("setter ") method. This method 
     * allows objects of other classes to be able to change the value of 
     * crabDefenseStatus
     */
    
    public void setCrabDefenseStatus( boolean crabDefenseStatus)
    { 
     //WHen the instance varible and lockal veriable have the same name 
     // precede the instace varible name with  'this. ' to disambuuate the otherwise 
     // identically named varibles. The keyword 'this' is a shortcut to a reference 
     // to the current object (in this case, to the current crabworld object)
     this.crabDefenseStatus = crabDefenseStatus;   
    } // end method setCrabDeenseStatus
    
    /**
     * This is an examoke of a mutator ("setter ") method. This method 
     * allows objects of other classes to be able to change the value of 
     * crabDefenseStatus
     */
    public boolean getCrabDefenseStatus()
    {
        return crabDefenseStatus; 
    }
} // end class CrabWorld

/* 
 * "Housekeeping" suggestion:
 * Please DELETE any older, unused code that you have 
 * commented-out; if you really need to refer to older code,
 * you can always review your commit history on Bitbucket
 */
