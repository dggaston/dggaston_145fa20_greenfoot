import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A lobster. Lobsters live on the beach. They like to eat crabs. (Well, in our game
 * they do...)
 * 
 * The lobster walks around randomly. If it runs into a crab it eats it.
 * In this version, we have added a sound effect, and the game stops when
 * a lobster eats the crab.
 * 
 * @author username@email.uscb.edu
 * @version 145fa20_lab3
 */
public class Lobster extends Actor
{
    /*FIELDS */
    private GreenfootImage image1; 
    private GreenfootImage image2;
    
    /*CONSTRUCTORS */
    public Lobster ()
    {
     image1 = new GreenfootImage ("lobster.png");
     image2 = new GreenfootImage ("lobster2.png");
    }
    /**
     * Do whatever lobsters do.
     */
    public void act()
    {
        turnAtEdge();
        randomTurn();
        randomTurnTowardsCenter(); /* for Exercise 4.28; see more below */
        move(5);
        lookForCrab();
    } // end method act

    /**
     * Check whether we are at the edge of the world. If we are, turn a bit.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() ) 
        {
            turn(17);
        } // end if
    } // end method turnAtEdge

    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by a random degree.
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10) 
        {
            turn(Greenfoot.getRandomNumber(91)-45);
        } // end if
    } // end method randomTurn

    /**
     * For Exercise 4.28:
     * Randomly decide to turn towards the center of the screen (where x = 280, y = 280).
     * Note that if this happens too often, the lobsters will all converge
     * in the middle of the screen. Thus, I coded this so that
     * it will happen only 2% of the time.
     */
    public void randomTurnTowardsCenter()
    {
        if (Greenfoot.getRandomNumber(100) < 2) 
        {
            turnTowards(280, 280);
        } // end if
    } // end method randomTurnTowardsCenter
    
    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if
    } // end method lookForCrab
} // end class Lobster

/* 
 * "Housekeeping" suggestion:
 * Please DELETE any older, unused code that you have 
 * commented-out; if you really need to refer to older code,
 * you can always review your commit history on Bitbucket
 */
