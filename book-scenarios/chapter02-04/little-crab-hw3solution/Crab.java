import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * In this version, the crab behaves as before, but we add animation of the 
 * image.
 * 
 * @author username@email.uscb.edu
 * @version 145fa20_lab3
 */
public class Crab extends Actor
{
    /* FIELD(S) */
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int frameCount; // could also name this "cycleCount" or something similar
    private int lobstersEaten; 
    private boolean setCrabDefenseStatus; 
    /* 
     * For Exercise 4.29: Add a time counter to the crab
     * (This must be declared as an instance variable; otherwise,
     *  there is no way the value of timeCounter can increment
     *  during each execution of the act() method!
     */
    // (NOTE: This was commented out and moved to CrabWorld class, as per Ex. 4.31)
    /*
    private int timeCounter; 
    */
   
    /* CONSTRUCTOR(S) */
    /**
     * Create a crab and initialize its two images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
        frameCount = 0;
        lobstersEaten = 0; 
        // For Exercise 4.29: Initialize the time counter to 0
        // (NOTE: Commented out and moved to CrabWorld class, as per Ex. 4.31)
        /*
        timeCounter = 0;
        */
    } // end Crab constructor

    /* METHOD(S) */
    /** 
     * Act - do whatever the crab wants to do. This method is called whenever
     *  the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        // For Exercise 4.29: Increment the time counter
        // (NOTE: Commented out and moved to CrabWorld class, as per Ex. 4.31)
        /*
        timeCounter = timeCounter + 1; // also could code as:
                                       // timeCounter++;
                                       // or
                                       // timeCounter += 1;
        */
       
        checkKeypress();
        move(5);
        lookForWorm();
        
        CrabWorld myCrabWorld = (CrabWorld)getWorld();
        if ( myCrabWorld.getCrabDefenseStatus() == false )
        {
          lookForWorm();   
        }// end if 
        switchImage();
    } // end method act
    
    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    {
        /* 
         * From Exercise 4.27: If you want to slow
         * down the animation, we can introduce code that will 
         * only allow the crab to animate every third time
         * the switchImage() method is called. To do this, we
         * introduce a counter variable 'frameCount' (which was
         * declared as an instance variable above), which we 
         * increment by one every time switchImage() is called...
         */
        frameCount = frameCount + 1;
        
        /* On the third cycle, animate the crab image and then
         * reset the counter variable back to zero. 
         * 
         * (NOTE: For faster animation, choose a threshold value less than 3; 
         * for slower animation, choose a value greater than 3.)
         */
        if (frameCount == 3) 
        {
            if (getImage() == image1) 
            {
                setImage(image2);
            }
            else
            {
                setImage(image1);
            } // end inner if-else
            
            // after animation, re-initialize frameCount to zero
            frameCount = 0;
        } // end outer if
  
    } // end method switchImage
            
    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    {
        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
        } // end if
        
        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);        
        } // end if
        
    } // end method checkKeyPress
     
    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing. If we have
     * eaten eight worms, we win.
     */
    public void lookForWorm()
    {
        if ( isTouching(Worm.class) ) 
        {
            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");
            
            wormsEaten = wormsEaten + 1;
            
            /* Play fanfare sound when the goal has been reached */
            if (wormsEaten == 8) 
            {
              CrabWorld myCrabWorld = (CrabWorld) getWorld(); 
              //getWorld().setCrabDefenseStatus( false );
            } // end inner if
            
        } // end outer if
    } // end lookForWorm
    
    /**
     * IF we are on offense, we can now eat Lobster objects 
     */
    public void lookForLobster()
    {
          if ( isTouching(Lobster.class) ) 
        {
            removeTouching(Lobster.class);
            Greenfoot.playSound("crunch.mp3");
            
            lobstersEaten++; 
            
            /* Play fanfare sound when the goal has been reached */
            if (lobstersEaten == 3) 
            {
             Greenfoot.playSound("fanfare.wav");
             Greenfoot.stop(); 
            } // end inner if
    }// end method loookForLobster
}
}
    
    


/* 
 * "Housekeeping" suggestion:
 * For the purpose of this homework, leave exercise 4.29 
 * "commented out" as above, but otherwise, please DELETE
 * any older, unused code that you have commented-out (e.g.,
 * the Crab class no longer needs the `randomTurn` method).
 * If you really need to refer to it again, you can always
 * review your commit history on Bitbucket
 */