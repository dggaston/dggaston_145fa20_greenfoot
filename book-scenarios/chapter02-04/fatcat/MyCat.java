import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * MyCat is your own cat. Get it to do things by writing code in its act method.
 * 
 * @author dggaston@email.sc.edu 
 * @version ICE for 9/18/2020
 */
public class MyCat extends Cat
{
    /**
     * Act - do whatever the MyCat wants to do.
     */
    public void act()
    {  
        /* 
         * Exercise 2.34
         */
        if  ( isSleepy() ); 
        { 
            sleep(5);
        }// end if 
        
        //Exercise 2.35
        if ( isBored() );
        {
            dance(); 
        }// end if 
        
        //Exercise 2.36
        if (isHungry());
        {
            eat();
        } // end if 
        
        
        // Exercise 2.37 Ensures that the cat will shout horray 
        shoutHooray() ; 
        
        
        // regardless of wether the cat was sleepy 
    }  // end method act   
}      // end class MyCat 
