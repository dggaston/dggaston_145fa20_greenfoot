import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ball here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ball extends Actor
{   
    /*Instance Varibles */
    /**
     * Excersise 4.36
     *  This Counts how often the ball has bounced off the edge.
     */
    private int numberOfBounces;  
    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
     move(4); // 4.35 Program the ball so it moves at at consistant speed 
     turnAtEdge();
     
    }    
    
    /**
     * 4.35 Program the ball to bounce off the edges of the world 
     * 4.39 Programm the bounce off the edge code so the balls bounce off the edge of the world at 
     * random angles
     */
    public void turnAtEdge()
    {
        if (isAtEdge())
        {
            turn(Greenfoot.getRandomNumber(181) - 90); // 4.39
            if (isAtEdge())
             {
             numberOfBounces = numberOfBounces + 1;     
             }// end if 
        } // end if 
    }// end method turnAtEdge
    
    
    
}
