import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class BallWorld here.
 * 
 * @author dggaston@email.uscb.edu
 * @version CSCI B145 Homework 3 part 2B
 */
public class BallWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public BallWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        
       /**
        * Excersise 4.37 Program your scenario so that
        * three balls are automatically present at the start 
        */ 
       
       
        Ball Ball1 = new Ball (); 
        addObject( Ball1 , Greenfoot.getRandomNumber(550)+20 ,Greenfoot.getRandomNumber(350)+20); 
        
        Ball Ball2 = new Ball();
        addObject (Ball2 , Greenfoot.getRandomNumber(550)+20 ,Greenfoot.getRandomNumber(350)+20);
        
        Ball Ball3 = new Ball(); 
        addObject (Ball3 , Greenfoot.getRandomNumber(550)+20 ,Greenfoot.getRandomNumber(350)+20);
        
        
    }// end constructor ballworld 
}// end class ballworld
