import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Lobster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lobster extends Actor
{
    /**
     * Act - do whatever the Lobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
      turnAtEdge();
      randomTurn();
      move(5);
      lookForCrab();
      turnTowards(); 
    } // end method act 
    
    /**
     * Make the lobsters turn toward the center of the screen occasionally.
     * Excercise 4.28 
     */
    public void turnTowards ()
    {
       if (Greenfoot.getRandomNumber(100) < 3 )
        {
            turnTowards(250,200); 
        } // end if   
    }// end method turn Towards 
    
     /**
     * Check if wether we have stubled upon a Crab.
     * If we have , eat it , play sound , and stop.
     * If not , do nothing. 
     */
    public void lookForCrab ()
    {

        if (isTouching(Crab.class) )
        {
            removeTouching( Crab.class ); 
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if  
    } // end method lookForCrab 

     /** 
     * If the Lobster reaches the edge of the screen, 
     * then have it turn before moving again 
     */
    
    public void turnAtEdge()
    {
        if (isAtEdge()) 
        {
            turn(17); 
        } // end if 

    }// end if
    
     /**
     * Make the Lobster turn by a random integer vlaue ranging 
     * between -45 and +45 degrees (Inclusively), 
     * 10 percent of the time (at random)
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10 )
        {
            turn( Greenfoot.getRandomNumber(91) - 45 ); 
        } // end if  

    } // end method randomTurn
} // end class Lobster 



