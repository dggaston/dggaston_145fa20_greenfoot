import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    /* INSTANCE VARIABLES (Fields)*/
    private GreenfootImage image1; // image 1 can be accessed anywhere in the class
    private GreenfootImage image2; // image2 can be accessed anywhere in the class
    private int wormsEaten; // instance varibles will have default value
    private int frameCount; // this keeps track of the number of cycles through 
    // the act method that have transpired since the crab's image was last updated.
   
    
    
    /*CONSTRUCTORS*/
    /**
     * Initialize a newly-instantaiated crab object with 
     * its two possible images; then set the initial image to 
     * one of those images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png"); 
        setImage(image1); 
        wormsEaten = 0;
        frameCount = 0;
      
    }// end Crab constructor

    /*METHODS*/

    public void act()
    {   checkKeypress(); 
        move(5); 
        lookForWorm();
        switchImage(); 
        
    } // end method act 

    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    {
        frameCount = frameCount =1; 

        if (frameCount == 3 )
        {
            if (getImage() == image1) 
            {
                setImage(image2);
            }
            else
            {
                setImage(image1);
            } // end inner if

            frameCount = 0;
        } // end if 
    } // end method switch image 

    /**
     * Checks for key pressed to control the movemnt of the crab 
     */
    public void checkKeypress()
    {
        if ( Greenfoot.isKeyDown ("left") )
        {
            turn(-4); 
        } //end if 

        if( Greenfoot.isKeyDown ("right") )
        {
            turn(4); 
        } //end if 
    } // end method checkKeyPress

    /**
     * Check if wether we have stubled upon a worm.
     * If we have , eat it and play sound. If not , do nothing. 
     */
    public void lookForWorm ()
    {

        if (isTouching(worm.class) )
        {
            removeTouching( worm.class ); 
            Greenfoot.playSound("slurp.wav");

            wormsEaten = wormsEaten +1;

            if (wormsEaten == 8 )
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            } // end INNER if
        } // end if  
    } // end method lookForWorm 

    /** 
     * If the crab reaches the edge of the screen, 
     * then have it turn before moving again 
     */

    public void turnAtEdge()
    {
        if (isAtEdge()) 
        {
            turn(17); 
        } // end if 

    }// end if

    /**
     * Make the crab turn by a random integer vlaue ranging 
     * between -45 and +45 degrees (Inclusively), 
     * 10 percent of the time (at random)
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10 )
        {
            turn( Greenfoot.getRandomNumber(91) - 45 ); 
        } // end if  

    } // end method randomTurn
} // end class Crab 

