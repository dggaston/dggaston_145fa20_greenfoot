import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    // CONSTRUCTOR
    public CrabWorld() 
    {
        
        // Set the size and resolution of the world 
        super(560, 560, 1);
        
        // Create a new Crab object and then assign a 
        // REFERENCE to that object to a varible of
        // the Crab data type 
        Crab myCrab = new Crab () ;
        // Add the object referenced by myCrab 
        // to the world at the indicated cordinates 
        addObject( myCrab , 250, 200 ) ;  
        
        
        // Ex 4.11
        Lobster lobster1 = new Lobster(); 
        addObject ( lobster1 , 480 , 420 ) ; 
        
        Lobster lobster2 = new Lobster(); 
        addObject ( lobster2 , 150 , 400 ) ; 
        
        Lobster lobster3 = new Lobster(); 
        addObject ( lobster3 , 400 , 100 ) ; 
        
        
        //4.12 
        worm worm1 = new worm ();
        addObject (worm1 , Greenfoot.getRandomNumber (531)+15 , 240 ) ; 
        
        worm worm2 = new worm ();
        addObject (worm2 , Greenfoot.getRandomNumber (531)+15 , 240 ) ;
        
        worm worm3 = new worm ();
        addObject (worm3 , Greenfoot.getRandomNumber (531)+15 , 240 ) ;
        
        // pulled from little crab 5 
        worm worm4 = new worm();
        addObject(worm4, 129, 488);
        worm worm5 = new worm();
        addObject(worm5, 254, 388);
        worm worm6 = new worm();
        addObject(worm6, 106, 334);
        worm worm7 = new worm();
        addObject(worm7, 338, 112);
        worm worm8 = new worm();
        addObject(worm8, 150, 94);
        worm worm9 = new worm();
        addObject(worm9, 373, 240);
        worm worm10 = new worm();
        addObject(worm10, 509, 55);
        
        //Excersise 4.31
        amountOfTime = 450; 
        
    } // end CrabWorld constructor 
    
    /**
     * Exercise 4.31
     * Moved the the time counter from the Crab Class to the CrabWorld class. 
     */
    private int amountOfTime; 
    // METHOD
    public void act()
    {
        amountOfTime = amountOfTime -1; 
        
        if (amountOfTime == 0 )
        {
           Greenfoot.playSound("GameOver.mp3"); 
           Greenfoot.stop();
        }// end if 
        
        //Excersise 4.34
        showText ("Time Left: "+amountOfTime , 100, 40);
        
    } // end method act 
    /**
     * Excersise 4.33 
     * Number of Parameters : 3
     * Type of Parameters: String, x-value, y-value
     * Return Type: Void 
     * Functons: 
     */ 
} // end class CrabWorld 








